pub const SUDOKU_SIZE: usize = 81;
pub const PEERS_SIZE: usize = 20;

pub struct Sudoku {
    pub values: [u32; SUDOKU_SIZE], // Values of the Sudoku, with 0 for unknown.
}

struct RowColumnGridIds {
    row: usize,
    column: usize,
    grid: usize,
}

impl Sudoku {
    /// Solves the Sudoku in place, using a simple backtracking algorithm.
    pub fn solve(
        self: &mut Self,
        peers: &[[usize; PEERS_SIZE]; SUDOKU_SIZE],
    ) -> Result<(), String> {
        return match self.values.iter().enumerate().find(|(_, &v)| v == 0) {
            None => Ok(()), // There are no unknown positions, the Sudoku is already solved.
            Some((index, _)) => {
                for guess in 1..10 {
                    if is_valid_guess(self, peers, index, guess) {
                        self.values[index] = guess; // Try a valid guess, and recur from the new position.
                        match self.solve(peers) {
                            Ok(()) => return Ok(()),
                            Err(_) => self.values[index] = 0, // Back track.
                        }
                    }
                }
                Err("This Sudoku is unsolvable from its given state.".to_string())
            }
        };
    }
}

/// Pre-computes the peers for all positions of the Sudoku.
///
/// For any positions i, j, in the Sudoku, j is a peer of i iff j != i AND
/// (row(i) == row(j) OR column(i) == column(j) OR grid(i) == grid(j)),
/// where grid refers to one of the nine 3x3 'regions' of the Sudoku puzzle.
pub fn compute_peers() -> [[usize; PEERS_SIZE]; SUDOKU_SIZE] {
    let mut peers: [[usize; PEERS_SIZE]; SUDOKU_SIZE] = [[0; PEERS_SIZE]; SUDOKU_SIZE];
    // Compute identifiers for the row, column, and grid for each position in the Sudoku.
    let rcg_ids = (0..SUDOKU_SIZE)
        .map(|i| {
            let r: usize = i / 9;
            let c: usize = i % 9;
            let g: usize = (r / 3) + ((c / 3) << 2);

            RowColumnGridIds {
                row: r,
                column: c,
                grid: g,
            }
        })
        .collect::<Vec<RowColumnGridIds>>();

    // For each position in the Sudoku, find the indices of its peers.
    for i in 0..SUDOKU_SIZE {
        let row = rcg_ids[i].row;
        let clm = rcg_ids[i].column;
        let grd = rcg_ids[i].grid;

        let mut j = 0usize;
        let mut p = 0usize;
        while j < SUDOKU_SIZE && p < PEERS_SIZE {
            if j != i
                && (row == rcg_ids[j].row || clm == rcg_ids[j].column || grd == rcg_ids[j].grid)
            {
                peers[i][p] = j;
                p += 1;
            }
            j += 1;
        }
    }
    peers
}

/// Determines whether the provided guess is valid for a given index, and a given state of the Sudoku.
/// A guess g for an index i is valid iff there is no p := peer(i) such that value(p) = g.
fn is_valid_guess(
    sud: &Sudoku,
    peers: &[[usize; PEERS_SIZE]; SUDOKU_SIZE],
    index: usize,
    guess: u32,
) -> bool {
    for i in 0..PEERS_SIZE {
        let p = peers[index][i];
        if sud.values[p] == guess {
            return false;
        }
    }
    true
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::reader;

    #[test]
    fn all_valid_and_complete() {
        let mut sudokus: Vec<Sudoku> = reader::parse_sudokus("resources/p096_sudoku.txt").unwrap();
        let peers = compute_peers();
        // All of the Sudokus should be valid at the starting state.
        assert!(sudokus.iter().all(|s| is_valid(s, &peers)));
        // None of the Sudokus should be complete to begin with.
        assert!(!sudokus.iter().any(|s| is_complete(s)));
        for s in &mut sudokus {
            s.solve(&peers).unwrap();
        }
        // All of the Sudokus should be complete once they have been solved.
        assert!(sudokus.iter().all(|s| is_complete(s)));
        // All of the Sudokus should be valid once they have been solved.
        assert!(sudokus.iter().all(|s| is_valid(s, &peers)));
    }

    fn is_complete(sud: &Sudoku) -> bool {
        !sud.values.iter().any(|&v| v == 0)
    }

    fn is_valid(sud: &Sudoku, peers: &[[usize; PEERS_SIZE]; SUDOKU_SIZE]) -> bool {
        sud.values
            .iter()
            .enumerate()
            .all(|(i, &v)| peers[i].iter().all(|&p| v == 0 || sud.values[p] != v))
    }
}
