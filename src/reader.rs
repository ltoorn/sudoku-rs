use std::fs::File;
use std::io::Error;
use std::io::Read;

use solver::{Sudoku, SUDOKU_SIZE};

enum State {
    TITLE,
    SUDOKU,
}

pub fn parse_sudokus(path: &str) -> Result<Vec<Sudoku>, String> {
    use reader::State::{SUDOKU, TITLE};

    let mut sudokus: Vec<Sudoku> = Vec::new();
    let mut sud: [u32; SUDOKU_SIZE] = [0; SUDOKU_SIZE];
    let mut index: usize = 0;
    let f_str = read(path).expect("Error while trying to read the file.");

    let mut state = TITLE;
    for c in f_str.chars() {
        match state {
            TITLE => {
                if c == '\n' {
                    state = SUDOKU;
                }
            }
            SUDOKU => {
                if c == 'G' {
                    assert_eq!(index, 81);
                    sudokus.push(Sudoku { values: sud });
                    index = 0;
                    state = TITLE;
                } else if c.is_ascii_digit() {
                    let n = c.to_digit(10).expect("Unable to extract digit.");
                    sud[index] = n;
                    index += 1;
                }
            }
        }
    }
    assert_eq!(index, 81);
    sudokus.push(Sudoku { values: sud });
    return Ok(sudokus);
}

fn read(path: &str) -> Result<String, Error> {
    let mut f = File::open(path)?;
    let mut s = String::new();
    f.read_to_string(&mut s)?;
    Ok(s)
}
