mod reader;
mod solver;

use solver::{Sudoku, PEERS_SIZE, SUDOKU_SIZE};

fn euler_sum(
    peers: &[[usize; PEERS_SIZE]; SUDOKU_SIZE],
    sudokus: Vec<Sudoku>,
) -> Result<u32, String> {
    let mut sum = 0;
    for mut sud in sudokus {
        match sud.solve(peers) {
            Ok(()) => (),
            Err(s) => return Err(s),
        };
        sum += (100 * sud.values[0]) + (10 * sud.values[1]) + sud.values[2];
    }
    return Ok(sum);
}

fn main() {
    use std::env;

    let args: Vec<String> = env::args().collect();
    if args.len() != 2 {
        println!("Usage: <path>");
        return ();
    }
    let path = &args[1];
    let peers = solver::compute_peers();
    let sudokus = match reader::parse_sudokus(path) {
        Ok(s) => s,
        Err(e) => {
            println!("Unable to parse Sudokus {}", e);
            return ();
        }
    };
    match euler_sum(&peers, sudokus) {
        Ok(sum) => println!("sum: {}.", sum),
        Err(e) => println!("{}.", e),
    }
}
